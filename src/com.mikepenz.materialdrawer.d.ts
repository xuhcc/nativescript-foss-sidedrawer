import androidviewView = android.view.View;
import androidwidgetImageView = android.widget.ImageView;
import androidgraphicsdrawableDrawable = android.graphics.drawable.Drawable;
import androidcontentContext = android.content.Context;
import javautilList = java.util.List;
import androidosBundle = android.os.Bundle;
/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.os.Bundle.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.widget.ImageView.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.AccountHeaderBuilder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.Drawer.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.interfaces.IProfile.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export class AccountHeader {
                public static NAVIGATION_DRAWER_ACCOUNT_ASPECT_RATIO: number;
                public static BUNDLE_SELECTION_HEADER: string;
                public mAccountHeaderBuilder: com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public removeProfile(param0: com.mikepenz.materialdrawer.model.interfaces.IProfile): void;
                public getProfiles(): javautilList;
                public saveInstanceState(param0: androidosBundle): androidosBundle;
                public setActiveProfile(param0: com.mikepenz.materialdrawer.model.interfaces.IProfile, param1: boolean): void;
                public setActiveProfile(param0: number, param1: boolean): void;
                public updateProfileByIdentifier(param0: com.mikepenz.materialdrawer.model.interfaces.IProfile): void;
                public getView(): androidviewView;
                public getHeaderBackgroundView(): androidwidgetImageView;
                public addProfile(param0: com.mikepenz.materialdrawer.model.interfaces.IProfile, param1: number): void;
                public setSelectionSecondLine(param0: string): void;
                public setDrawer(param0: com.mikepenz.materialdrawer.Drawer): void;
                public setActiveProfile(param0: com.mikepenz.materialdrawer.model.interfaces.IProfile): void;
                public setProfiles(param0: javautilList): void;
                public isSelectionListShown(): boolean;
                public setSelectionSecondLineShown(param0: boolean): void;
                public removeProfileByIdentifier(param0: number): void;
                public setHeaderBackground(param0: com.mikepenz.materialdrawer.holder.ImageHolder): void;
                public setActiveProfile(param0: number): void;
                public getActiveProfile(): com.mikepenz.materialdrawer.model.interfaces.IProfile;
                public clear(): void;
                public setBackground(param0: androidgraphicsdrawableDrawable): void;
                public setSelectionFirstLineShown(param0: boolean): void;
                public toggleSelectionList(param0: androidcontentContext): void;
                public removeProfile(param0: number): void;
                public setBackgroundRes(param0: number): void;
                public getAccountHeaderBuilder(): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public setSelectionFirstLine(param0: string): void;
                public addProfiles(param0: native.Array<com.mikepenz.materialdrawer.model.interfaces.IProfile>): void;
                public constructor(param0: com.mikepenz.materialdrawer.AccountHeaderBuilder);
                public updateProfile(param0: com.mikepenz.materialdrawer.model.interfaces.IProfile): void;
            }
            export namespace AccountHeader {
                export class OnAccountHeaderItemLongClickListener {
                    /**
                     * Constructs a new instance of the com.mikepenz.materialdrawer.AccountHeader$OnAccountHeaderItemLongClickListener interface with the provided implementation.
                     */
                    public constructor(implementation: {
                        onProfileLongClick(param0: androidviewView, param1: com.mikepenz.materialdrawer.model.interfaces.IProfile, param2: boolean): boolean;
                    });
                    public onProfileLongClick(param0: androidviewView, param1: com.mikepenz.materialdrawer.model.interfaces.IProfile, param2: boolean): boolean;
                }
                export class OnAccountHeaderListener {
                    /**
                     * Constructs a new instance of the com.mikepenz.materialdrawer.AccountHeader$OnAccountHeaderListener interface with the provided implementation.
                     */
                    public constructor(implementation: {
                        onProfileChanged(param0: androidviewView, param1: com.mikepenz.materialdrawer.model.interfaces.IProfile, param2: boolean): boolean;
                    });
                    public onProfileChanged(param0: androidviewView, param1: com.mikepenz.materialdrawer.model.interfaces.IProfile, param2: boolean): boolean;
                }
                export class OnAccountHeaderProfileImageListener {
                    /**
                     * Constructs a new instance of the com.mikepenz.materialdrawer.AccountHeader$OnAccountHeaderProfileImageListener interface with the provided implementation.
                     */
                    public constructor(implementation: {
                        onProfileImageClick(param0: androidviewView, param1: com.mikepenz.materialdrawer.model.interfaces.IProfile, param2: boolean): boolean;
                        onProfileImageLongClick(param0: androidviewView, param1: com.mikepenz.materialdrawer.model.interfaces.IProfile, param2: boolean): boolean;
                    });
                    public onProfileImageClick(param0: androidviewView, param1: com.mikepenz.materialdrawer.model.interfaces.IProfile, param2: boolean): boolean;
                    public onProfileImageLongClick(param0: androidviewView, param1: com.mikepenz.materialdrawer.model.interfaces.IProfile, param2: boolean): boolean;
                }
                export class OnAccountHeaderSelectionViewClickListener {
                    /**
                     * Constructs a new instance of the com.mikepenz.materialdrawer.AccountHeader$OnAccountHeaderSelectionViewClickListener interface with the provided implementation.
                     */
                    public constructor(implementation: {
                        onClick(param0: androidviewView, param1: com.mikepenz.materialdrawer.model.interfaces.IProfile): boolean;
                    });
                    public onClick(param0: androidviewView, param1: com.mikepenz.materialdrawer.model.interfaces.IProfile): boolean;
                }
            }
        }
    }
}

import androidappActivity = android.app.Activity;
import androidgraphicsTypeface = android.graphics.Typeface;
import androidwidgetImageViewScaleType = android.widget.ImageView.ScaleType;
import androidwidgetTextView = android.widget.TextView;
import javalangBoolean = java.lang.Boolean;
/// <reference path="./android.app.Activity.d.ts" />
/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.os.Bundle.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.widget.ImageView.d.ts" />
/// <reference path="./android.widget.TextView.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.AccountHeader.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.Drawer.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ColorHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.DimenHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.interfaces.IProfile.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.view.BezelImageView.d.ts" />
/// <reference path="./java.lang.Boolean.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export class AccountHeaderBuilder {
                public mAccountHeader: androidviewView;
                public mAccountHeaderBackground: androidwidgetImageView;
                public mCurrentProfileView: com.mikepenz.materialdrawer.view.BezelImageView;
                public mAccountHeaderTextSection: androidviewView;
                public mAccountSwitcherArrow: androidwidgetImageView;
                public mCurrentProfileName: androidwidgetTextView;
                public mCurrentProfileEmail: androidwidgetTextView;
                public mProfileFirstView: com.mikepenz.materialdrawer.view.BezelImageView;
                public mProfileSecondView: com.mikepenz.materialdrawer.view.BezelImageView;
                public mProfileThirdView: com.mikepenz.materialdrawer.view.BezelImageView;
                public mCurrentProfile: com.mikepenz.materialdrawer.model.interfaces.IProfile;
                public mProfileFirst: com.mikepenz.materialdrawer.model.interfaces.IProfile;
                public mProfileSecond: com.mikepenz.materialdrawer.model.interfaces.IProfile;
                public mProfileThird: com.mikepenz.materialdrawer.model.interfaces.IProfile;
                public mSelectionListShown: boolean;
                public mAccountHeaderTextSectionBackgroundResource: number;
                public mActivity: androidappActivity;
                public mCompactStyle: boolean;
                public mTypeface: androidgraphicsTypeface;
                public mNameTypeface: androidgraphicsTypeface;
                public mEmailTypeface: androidgraphicsTypeface;
                public mHeight: com.mikepenz.materialdrawer.holder.DimenHolder;
                public mTextColor: com.mikepenz.materialdrawer.holder.ColorHolder;
                public mCurrentHiddenInList: boolean;
                public mSelectionFirstLineShown: boolean;
                public mSelectionSecondLineShown: boolean;
                public mSelectionFirstLine: string;
                public mSelectionSecondLine: string;
                public mPaddingBelowHeader: boolean;
                public mDividerBelowHeader: boolean;
                public mTranslucentStatusBar: boolean;
                public mHeaderBackground: com.mikepenz.materialdrawer.holder.ImageHolder;
                public mHeaderBackgroundScaleType: androidwidgetImageViewScaleType;
                public mProfileImagesVisible: boolean;
                public mOnlyMainProfileImageVisible: boolean;
                public mOnlySmallProfileImagesVisible: boolean;
                public mCloseDrawerOnProfileListClick: javalangBoolean;
                public mResetDrawerOnProfileListClick: boolean;
                public mProfileImagesClickable: boolean;
                public mAlternativeProfileHeaderSwitching: boolean;
                public mThreeSmallProfileImages: boolean;
                public mOnProfileClickDrawerCloseDelay: number;
                public mOnAccountHeaderProfileImageListener: com.mikepenz.materialdrawer.AccountHeader.OnAccountHeaderProfileImageListener;
                public mOnAccountHeaderSelectionViewClickListener: com.mikepenz.materialdrawer.AccountHeader.OnAccountHeaderSelectionViewClickListener;
                public mSelectionListEnabledForSingleProfile: boolean;
                public mSelectionListEnabled: boolean;
                public mAccountHeaderContainer: androidviewView;
                public mProfiles: javautilList;
                public mOnAccountHeaderListener: com.mikepenz.materialdrawer.AccountHeader.OnAccountHeaderListener;
                public mOnAccountHeaderItemLongClickListener: com.mikepenz.materialdrawer.AccountHeader.OnAccountHeaderItemLongClickListener;
                public mDrawer: com.mikepenz.materialdrawer.Drawer;
                public mSavedInstance: androidosBundle;
                public withCompactStyle(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withHeaderBackground(param0: androidgraphicsdrawableDrawable): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public getCurrentSelection(): number;
                public constructor();
                public withHeaderBackgroundScaleType(param0: androidwidgetImageViewScaleType): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public updateHeaderAndList(): void;
                public withSelectionSecondLine(param0: string): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public calculateProfiles(): void;
                public switchProfiles(param0: com.mikepenz.materialdrawer.model.interfaces.IProfile): boolean;
                public withOnlySmallProfileImagesVisible(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withSelectionFirstLineShown(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withTranslucentStatusBar(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public buildDrawerSelectionList(): void;
                public withCurrentProfileHiddenInList(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withSelectionListEnabledForSingleProfile(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public buildProfiles(): void;
                public withHeaderBackground(param0: com.mikepenz.materialdrawer.holder.ImageHolder): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public toggleSelectionList(param0: androidcontentContext): void;
                public withSelectionSecondLineShown(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withResetDrawerOnProfileListClick(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withThreeSmallProfileImages(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public addProfiles(param0: native.Array<com.mikepenz.materialdrawer.model.interfaces.IProfile>): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withSavedInstance(param0: androidosBundle): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withCloseDrawerOnProfileListClick(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withProfiles(param0: javautilList): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withOnlyMainProfileImageVisible(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withTextColorRes(param0: number): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withSelectionFistLineShown(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withHeightPx(param0: number): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withTypeface(param0: androidgraphicsTypeface): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withHeaderBackground(param0: number): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withProfileImagesVisible(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public build(): com.mikepenz.materialdrawer.AccountHeader;
                public withOnAccountHeaderProfileImageListener(param0: com.mikepenz.materialdrawer.AccountHeader.OnAccountHeaderProfileImageListener): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withTextColor(param0: number): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withDividerBelowHeader(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withHeightRes(param0: number): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withAccountHeader(param0: number): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withPaddingBelowHeader(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withNameTypeface(param0: androidgraphicsTypeface): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withOnAccountHeaderListener(param0: com.mikepenz.materialdrawer.AccountHeader.OnAccountHeaderListener): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public onProfileClick(param0: androidviewView, param1: boolean): void;
                public withSelectionFirstLine(param0: string): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withProfileImagesClickable(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withAlternativeProfileHeaderSwitching(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withAccountHeader(param0: androidviewView): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withOnAccountHeaderItemLongClickListener(param0: com.mikepenz.materialdrawer.AccountHeader.OnAccountHeaderItemLongClickListener): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withOnAccountHeaderSelectionViewClickListener(param0: com.mikepenz.materialdrawer.AccountHeader.OnAccountHeaderSelectionViewClickListener): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withActivity(param0: androidappActivity): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withSelectionListEnabled(param0: boolean): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withOnProfileClickDrawerCloseDelay(param0: number): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withHeightDp(param0: number): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withEmailTypeface(param0: androidgraphicsTypeface): com.mikepenz.materialdrawer.AccountHeaderBuilder;
                public withDrawer(param0: com.mikepenz.materialdrawer.Drawer): com.mikepenz.materialdrawer.AccountHeaderBuilder;
            }
        }
    }
}

declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export class BuildConfig {
                public static DEBUG: boolean;
                public static APPLICATION_ID: string;
                public static BUILD_TYPE: string;
                public static FLAVOR: string;
                public static VERSION_CODE: number;
                public static VERSION_NAME: string;
                public constructor();
            }
        }
    }
}

import androidsupportv4widgetDrawerLayout = android.support.v4.widget.DrawerLayout;
import androidsupportv7widgetToolbar = android.support.v7.widget.Toolbar;
import androidsupportv7appActionBarDrawerToggle = android.support.v7.app.ActionBarDrawerToggle;
import androidwidgetFrameLayout = android.widget.FrameLayout;
import androidsupportv7widgetRecyclerView = android.support.v7.widget.RecyclerView;
import javalangObject = java.lang.Object;
import androidwidgetAdapterView = android.widget.AdapterView;
/// <reference path="./android.app.Activity.d.ts" />
/// <reference path="./android.os.Bundle.d.ts" />
/// <reference path="./android.support.v4.widget.DrawerLayout.d.ts" />
/// <reference path="./android.support.v7.app.ActionBarDrawerToggle.d.ts" />
/// <reference path="./android.support.v7.widget.RecyclerView.d.ts" />
/// <reference path="./android.support.v7.widget.Toolbar.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.widget.AdapterView.d.ts" />
/// <reference path="./android.widget.FrameLayout.d.ts" />
/// <reference path="./com.mikepenz.fastadapter.FastAdapter.d.ts" />
/// <reference path="./com.mikepenz.fastadapter.adapters.ModelAdapter.d.ts" />
/// <reference path="./com.mikepenz.fastadapter.expandable.ExpandableExtension.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.DrawerBuilder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.MiniDrawer.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.DimenHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.interfaces.IDrawerItem.d.ts" />
/// <reference path="./com.mikepenz.materialize.Materialize.d.ts" />
/// <reference path="./com.mikepenz.materialize.view.ScrimInsetsRelativeLayout.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export class Drawer {
                public static BUNDLE_SELECTION: string;
                public static BUNDLE_SELECTION_APPENDED: string;
                public static BUNDLE_STICKY_FOOTER_SELECTION: string;
                public static BUNDLE_STICKY_FOOTER_SELECTION_APPENDED: string;
                public static BUNDLE_DRAWER_CONTENT_SWITCHED: string;
                public static BUNDLE_DRAWER_CONTENT_SWITCHED_APPENDED: string;
                public static PREF_USER_LEARNED_DRAWER: string;
                public static PREF_USER_OPENED_DRAWER_BY_DRAGGING: string;
                public mDrawerBuilder: com.mikepenz.materialdrawer.DrawerBuilder;
                public updateName(param0: number, param1: com.mikepenz.materialdrawer.holder.StringHolder): void;
                public setHeader(param0: androidviewView): void;
                public updateStickyFooterItemAtPosition(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem, param1: number): void;
                public getCurrentStickyFooterSelectedPosition(): number;
                public saveInstanceState(param0: androidosBundle): androidosBundle;
                public getDrawerItem(param0: javalangObject): com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
                public getAdapter(): com.mikepenz.fastadapter.FastAdapter;
                public setStickyFooterItemAtPosition(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem, param1: number): void;
                public getStickyFooterPosition(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): number;
                public switchedDrawerContent(): boolean;
                public getCurrentSelection(): number;
                public addItem(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): void;
                public setOnDrawerItemLongClickListener(param0: com.mikepenz.materialdrawer.Drawer.OnDrawerItemLongClickListener): void;
                public removeStickyFooterItemAtPosition(param0: number): void;
                public getContent(): androidwidgetFrameLayout;
                public getStickyHeader(): androidviewView;
                public getPosition(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): number;
                public getHeader(): androidviewView;
                public addStickyFooterItemAtPosition(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem, param1: number): void;
                public setSelection(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem, param1: boolean): void;
                public getMaterialize(): com.mikepenz.materialize.Materialize;
                public setStickyFooterSelectionAtPosition(param0: number): void;
                public setItems(param0: javautilList): void;
                public getActionBarDrawerToggle(): androidsupportv7appActionBarDrawerToggle;
                public setOnDrawerItemClickListener(param0: com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener): void;
                public setSelection(param0: number): void;
                public updateItemAtPosition(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem, param1: number): void;
                public isDrawerOpen(): boolean;
                public updateStickyFooterItem(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): void;
                public getPosition(param0: number): number;
                public deselect(): void;
                public getItemAdapter(): com.mikepenz.fastadapter.adapters.ModelAdapter;
                public getDrawerBuilder(): com.mikepenz.materialdrawer.DrawerBuilder;
                public updateIcon(param0: number, param1: com.mikepenz.materialdrawer.holder.ImageHolder): void;
                public getDrawerItem(param0: number): com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
                public getExpandableExtension(): com.mikepenz.fastadapter.expandable.ExpandableExtension;
                public getStickyFooterPosition(param0: number): number;
                public removeHeader(): void;
                public setSelectionAtPosition(param0: number, param1: boolean): boolean;
                public getOriginalDrawerItems(): javautilList;
                public setToolbar(param0: androidappActivity, param1: androidsupportv7widgetToolbar): void;
                public getStickyFooter(): androidviewView;
                public constructor(param0: com.mikepenz.materialdrawer.DrawerBuilder);
                public setToolbar(param0: androidappActivity, param1: androidsupportv7widgetToolbar, param2: boolean): void;
                public getHeaderAdapter(): com.mikepenz.fastadapter.adapters.ModelAdapter;
                public updateItem(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): void;
                public addItemAtPosition(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem, param1: number): void;
                public closeDrawer(): void;
                public setHeader(param0: androidviewView, param1: boolean, param2: boolean): void;
                public setItemAtPosition(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem, param1: number): void;
                public getFooterAdapter(): com.mikepenz.fastadapter.adapters.ModelAdapter;
                public removeItem(param0: number): void;
                public getFooter(): androidviewView;
                public removeItems(param0: native.Array<number>): void;
                public getOnDrawerItemLongClickListener(): com.mikepenz.materialdrawer.Drawer.OnDrawerItemLongClickListener;
                public setFullscreen(param0: boolean): void;
                public setStickyFooterSelectionAtPosition(param0: number, param1: boolean): void;
                public getCurrentSelectedPosition(): number;
                public addItemsAtPosition(param0: number, param1: native.Array<com.mikepenz.materialdrawer.model.interfaces.IDrawerItem>): void;
                public addItems(param0: native.Array<com.mikepenz.materialdrawer.model.interfaces.IDrawerItem>): void;
                public setGravity(param0: number): void;
                public setSelection(param0: number, param1: boolean): void;
                public removeItemByPosition(param0: number): void;
                public openDrawer(): void;
                public deselect(param0: number): void;
                public removeAllStickyFooterItems(): void;
                public removeAllItems(): void;
                public setHeader(param0: androidviewView, param1: boolean): void;
                public getDrawerItems(): javautilList;
                public resetDrawerContent(): void;
                public setActionBarDrawerToggle(param0: androidsupportv7appActionBarDrawerToggle): void;
                public switchDrawerContent(param0: com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener, param1: com.mikepenz.materialdrawer.Drawer.OnDrawerItemLongClickListener, param2: javautilList, param3: number): void;
                public updateBadge(param0: number, param1: com.mikepenz.materialdrawer.holder.StringHolder): void;
                public getDrawerLayout(): androidsupportv4widgetDrawerLayout;
                public getRecyclerView(): androidsupportv7widgetRecyclerView;
                public setSelectionAtPosition(param0: number): boolean;
                public getSlider(): com.mikepenz.materialize.view.ScrimInsetsRelativeLayout;
                public getOnDrawerNavigationListener(): com.mikepenz.materialdrawer.Drawer.OnDrawerNavigationListener;
                public setStickyFooterSelection(param0: number, param1: boolean): void;
                public getMiniDrawer(): com.mikepenz.materialdrawer.MiniDrawer;
                public setOnDrawerNavigationListener(param0: com.mikepenz.materialdrawer.Drawer.OnDrawerNavigationListener): void;
                public getOnDrawerItemClickListener(): com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener;
                public setSelection(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): void;
                public addStickyFooterItem(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): void;
                public setHeader(param0: androidviewView, param1: boolean, param2: boolean, param3: com.mikepenz.materialdrawer.holder.DimenHolder): void;
            }
            export namespace Drawer {
                export class OnDrawerItemClickListener {
                    /**
                     * Constructs a new instance of the com.mikepenz.materialdrawer.Drawer$OnDrawerItemClickListener interface with the provided implementation.
                     */
                    public constructor(implementation: {
                        onItemClick(param0: androidviewView, param1: number, param2: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): boolean;
                    });
                    public onItemClick(param0: androidviewView, param1: number, param2: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): boolean;
                }
                export class OnDrawerItemLongClickListener {
                    /**
                     * Constructs a new instance of the com.mikepenz.materialdrawer.Drawer$OnDrawerItemLongClickListener interface with the provided implementation.
                     */
                    public constructor(implementation: {
                        onItemLongClick(param0: androidviewView, param1: number, param2: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): boolean;
                    });
                    public onItemLongClick(param0: androidviewView, param1: number, param2: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): boolean;
                }
                export class OnDrawerItemSelectedListener {
                    /**
                     * Constructs a new instance of the com.mikepenz.materialdrawer.Drawer$OnDrawerItemSelectedListener interface with the provided implementation.
                     */
                    public constructor(implementation: {
                        onItemSelected(param0: androidwidgetAdapterView, param1: androidviewView, param2: number, param3: number, param4: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): void;
                        onNothingSelected(param0: androidwidgetAdapterView): void;
                    });
                    public onNothingSelected(param0: androidwidgetAdapterView): void;
                    public onItemSelected(param0: androidwidgetAdapterView, param1: androidviewView, param2: number, param3: number, param4: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): void;
                }
                export class OnDrawerListener {
                    /**
                     * Constructs a new instance of the com.mikepenz.materialdrawer.Drawer$OnDrawerListener interface with the provided implementation.
                     */
                    public constructor(implementation: {
                        onDrawerOpened(param0: androidviewView): void;
                        onDrawerClosed(param0: androidviewView): void;
                        onDrawerSlide(param0: androidviewView, param1: number): void;
                    });
                    public onDrawerClosed(param0: androidviewView): void;
                    public onDrawerOpened(param0: androidviewView): void;
                    public onDrawerSlide(param0: androidviewView, param1: number): void;
                }
                export class OnDrawerNavigationListener {
                    /**
                     * Constructs a new instance of the com.mikepenz.materialdrawer.Drawer$OnDrawerNavigationListener interface with the provided implementation.
                     */
                    public constructor(implementation: {
                        onNavigationClickListener(param0: androidviewView): boolean;
                    });
                    public onNavigationClickListener(param0: androidviewView): boolean;
                }
            }
        }
    }
}

import androidviewViewGroup = android.view.ViewGroup;
import androidsupportv7widgetRecyclerViewAdapter = android.support.v7.widget.RecyclerView.Adapter;
import androidsupportv7widgetRecyclerViewItemAnimator = android.support.v7.widget.RecyclerView.ItemAnimator;
import androidsupportv7widgetRecyclerViewLayoutManager = android.support.v7.widget.RecyclerView.LayoutManager;
import javalangInteger = java.lang.Integer;
/// <reference path="./android.app.Activity.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.os.Bundle.d.ts" />
/// <reference path="./android.support.v4.widget.DrawerLayout.d.ts" />
/// <reference path="./android.support.v7.app.ActionBarDrawerToggle.d.ts" />
/// <reference path="./android.support.v7.widget.RecyclerView.d.ts" />
/// <reference path="./android.support.v7.widget.Toolbar.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.fastadapter.FastAdapter.d.ts" />
/// <reference path="./com.mikepenz.fastadapter.IItemAdapter.d.ts" />
/// <reference path="./com.mikepenz.fastadapter.adapters.ModelAdapter.d.ts" />
/// <reference path="./com.mikepenz.fastadapter.expandable.ExpandableExtension.d.ts" />
/// <reference path="./com.mikepenz.fastadapter.utils.DefaultIdDistributor.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.AccountHeader.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.Drawer.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.MiniDrawer.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.DimenHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.interfaces.IDrawerItem.d.ts" />
/// <reference path="./com.mikepenz.materialize.Materialize.d.ts" />
/// <reference path="./com.mikepenz.materialize.view.ScrimInsetsRelativeLayout.d.ts" />
/// <reference path="./java.lang.Boolean.d.ts" />
/// <reference path="./java.lang.Integer.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export class DrawerBuilder {
                public mUsed: boolean;
                public mCurrentStickyFooterSelection: number;
                public mAppended: boolean;
                public mActivity: androidappActivity;
                public mLayoutManager: androidsupportv7widgetRecyclerViewLayoutManager;
                public mRootView: androidviewViewGroup;
                public mMaterialize: com.mikepenz.materialize.Materialize;
                public idDistributor: com.mikepenz.fastadapter.utils.DefaultIdDistributor;
                public mTranslucentStatusBar: boolean;
                public mDisplayBelowStatusBar: javalangBoolean;
                public mToolbar: androidsupportv7widgetToolbar;
                public mTranslucentNavigationBar: boolean;
                public mTranslucentNavigationBarProgrammatically: boolean;
                public mFullscreen: boolean;
                public mSystemUIHidden: boolean;
                public mCustomView: androidviewView;
                public mDrawerLayout: androidsupportv4widgetDrawerLayout;
                public mSliderLayout: com.mikepenz.materialize.view.ScrimInsetsRelativeLayout;
                public mSliderBackgroundColor: number;
                public mSliderBackgroundColorRes: number;
                public mSliderBackgroundDrawable: androidgraphicsdrawableDrawable;
                public mSliderBackgroundDrawableRes: number;
                public mDrawerWidth: number;
                public mDrawerGravity: javalangInteger;
                public mAccountHeader: com.mikepenz.materialdrawer.AccountHeader;
                public mAccountHeaderSticky: boolean;
                public mAnimateActionBarDrawerToggle: boolean;
                public mActionBarDrawerToggleEnabled: boolean;
                public mActionBarDrawerToggle: androidsupportv7appActionBarDrawerToggle;
                public mScrollToTopAfterClick: boolean;
                public mHeaderView: androidviewView;
                public mHeaderDivider: boolean;
                public mHeaderPadding: boolean;
                public mHeiderHeight: com.mikepenz.materialdrawer.holder.DimenHolder;
                public mStickyHeaderView: androidviewView;
                public mStickyHeaderShadow: boolean;
                public mFooterView: androidviewView;
                public mFooterDivider: boolean;
                public mFooterClickable: boolean;
                public mStickyFooterView: androidviewViewGroup;
                public mStickyFooterDivider: boolean;
                public mStickyFooterShadowView: androidviewView;
                public mStickyFooterShadow: boolean;
                public mFireInitialOnClick: boolean;
                public mMultiSelect: boolean;
                public mSelectedItemPosition: number;
                public mSelectedItemIdentifier: number;
                public mRecyclerView: androidsupportv7widgetRecyclerView;
                public mHasStableIds: boolean;
                public mAdapter: com.mikepenz.fastadapter.FastAdapter;
                public mHeaderAdapter: com.mikepenz.fastadapter.adapters.ModelAdapter;
                public mItemAdapter: com.mikepenz.fastadapter.adapters.ModelAdapter;
                public mFooterAdapter: com.mikepenz.fastadapter.adapters.ModelAdapter;
                public mExpandableExtension: com.mikepenz.fastadapter.expandable.ExpandableExtension;
                public mAdapterWrapper: androidsupportv7widgetRecyclerViewAdapter;
                public mItemAnimator: androidsupportv7widgetRecyclerViewItemAnimator;
                public mKeepStickyItemsVisible: boolean;
                public mStickyDrawerItems: javautilList;
                public mCloseOnClick: boolean;
                public mDelayOnDrawerClose: number;
                public mDelayDrawerClickEvent: number;
                public mOnDrawerListener: com.mikepenz.materialdrawer.Drawer.OnDrawerListener;
                public mOnDrawerItemClickListener: com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener;
                public mOnDrawerItemLongClickListener: com.mikepenz.materialdrawer.Drawer.OnDrawerItemLongClickListener;
                public mOnDrawerNavigationListener: com.mikepenz.materialdrawer.Drawer.OnDrawerNavigationListener;
                public mShowDrawerOnFirstLaunch: boolean;
                public mShowDrawerUntilDraggedOpened: boolean;
                public mGenerateMiniDrawer: boolean;
                public mMiniDrawer: com.mikepenz.materialdrawer.MiniDrawer;
                public mSavedInstance: androidosBundle;
                public withHeader(param0: androidviewView): com.mikepenz.materialdrawer.DrawerBuilder;
                public withFooterClickable(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withRootView(param0: androidviewViewGroup): com.mikepenz.materialdrawer.DrawerBuilder;
                public withHasStableIds(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public getAdapter(): com.mikepenz.fastadapter.FastAdapter;
                public withTranslucentStatusBar(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public build(): com.mikepenz.materialdrawer.Drawer;
                public withSliderBackgroundDrawableRes(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public withActionBarDrawerToggle(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public constructor();
                public withDrawerWidthPx(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public withItemAnimator(param0: androidsupportv7widgetRecyclerViewItemAnimator): com.mikepenz.materialdrawer.DrawerBuilder;
                public withDrawerGravity(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public withActionBarDrawerToggleAnimated(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withOnDrawerNavigationListener(param0: com.mikepenz.materialdrawer.Drawer.OnDrawerNavigationListener): com.mikepenz.materialdrawer.DrawerBuilder;
                public withShowDrawerOnFirstLaunch(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withStickyHeaderShadow(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withDrawerItems(param0: javautilList): com.mikepenz.materialdrawer.DrawerBuilder;
                public withHeader(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public withShowDrawerUntilDraggedOpened(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withGenerateMiniDrawer(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withActivity(param0: androidappActivity): com.mikepenz.materialdrawer.DrawerBuilder;
                public withHeaderPadding(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withFullscreen(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withScrollToTopAfterClick(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withStickyFooterShadow(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withTranslucentNavigationBarProgrammatically(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withRootView(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public withDisplayBelowStatusBar(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withTranslucentNavigationBar(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public addDrawerItems(param0: native.Array<com.mikepenz.materialdrawer.model.interfaces.IDrawerItem>): com.mikepenz.materialdrawer.DrawerBuilder;
                public getHeaderAdapter(): com.mikepenz.fastadapter.IItemAdapter;
                public withCloseOnClick(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withDelayOnDrawerClose(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public withStickyFooter(param0: androidviewViewGroup): com.mikepenz.materialdrawer.DrawerBuilder;
                public getDrawerItem(param0: number): com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
                public inflateMenu(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public append(param0: com.mikepenz.materialdrawer.Drawer): com.mikepenz.materialdrawer.Drawer;
                public withSelectedItem(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public withSliderBackgroundColor(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public withDrawerWidthDp(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public buildForFragment(): com.mikepenz.materialdrawer.Drawer;
                public withSelectedItemByPosition(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public withSystemUIHidden(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withDrawerLayout(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public withAdapter(param0: com.mikepenz.fastadapter.FastAdapter): com.mikepenz.materialdrawer.DrawerBuilder;
                public withKeepStickyItemsVisible(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public checkDrawerItem(param0: number, param1: boolean): boolean;
                public withToolbar(param0: androidsupportv7widgetToolbar): com.mikepenz.materialdrawer.DrawerBuilder;
                public getItemAdapter(): com.mikepenz.fastadapter.IItemAdapter;
                public withFooter(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public withDrawerWidthRes(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public withFireOnInitialOnClick(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withStickyDrawerItems(param0: javautilList): com.mikepenz.materialdrawer.DrawerBuilder;
                public closeDrawerDelayed(): void;
                public withStickyHeader(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public withDelayDrawerClickEvent(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public withAccountHeader(param0: com.mikepenz.materialdrawer.AccountHeader, param1: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withActionBarDrawerToggle(param0: androidsupportv7appActionBarDrawerToggle): com.mikepenz.materialdrawer.DrawerBuilder;
                public withAdapterWrapper(param0: androidsupportv7widgetRecyclerViewAdapter): com.mikepenz.materialdrawer.DrawerBuilder;
                public withSliderBackgroundDrawable(param0: androidgraphicsdrawableDrawable): com.mikepenz.materialdrawer.DrawerBuilder;
                public withAccountHeader(param0: com.mikepenz.materialdrawer.AccountHeader): com.mikepenz.materialdrawer.DrawerBuilder;
                public withFooterDivider(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withHeaderDivider(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withOnDrawerItemLongClickListener(param0: com.mikepenz.materialdrawer.Drawer.OnDrawerItemLongClickListener): com.mikepenz.materialdrawer.DrawerBuilder;
                public withStickyFooterDivider(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withCustomView(param0: androidviewView): com.mikepenz.materialdrawer.DrawerBuilder;
                public withStickyFooter(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
                public buildView(): com.mikepenz.materialdrawer.Drawer;
                public addStickyDrawerItems(param0: native.Array<com.mikepenz.materialdrawer.model.interfaces.IDrawerItem>): com.mikepenz.materialdrawer.DrawerBuilder;
                public withOnDrawerListener(param0: com.mikepenz.materialdrawer.Drawer.OnDrawerListener): com.mikepenz.materialdrawer.DrawerBuilder;
                public resetStickyFooterSelection(): void;
                public handleDrawerNavigation(param0: androidappActivity, param1: boolean): void;
                public withInnerShadow(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withHeaderHeight(param0: com.mikepenz.materialdrawer.holder.DimenHolder): com.mikepenz.materialdrawer.DrawerBuilder;
                public withDrawerLayout(param0: androidsupportv4widgetDrawerLayout): com.mikepenz.materialdrawer.DrawerBuilder;
                public withMultiSelect(param0: boolean): com.mikepenz.materialdrawer.DrawerBuilder;
                public withStickyHeader(param0: androidviewView): com.mikepenz.materialdrawer.DrawerBuilder;
                public withFooter(param0: androidviewView): com.mikepenz.materialdrawer.DrawerBuilder;
                public getFooterAdapter(): com.mikepenz.fastadapter.IItemAdapter;
                public withSavedInstance(param0: androidosBundle): com.mikepenz.materialdrawer.DrawerBuilder;
                public withRecyclerView(param0: androidsupportv7widgetRecyclerView): com.mikepenz.materialdrawer.DrawerBuilder;
                public constructor(param0: androidappActivity);
                public withOnDrawerItemClickListener(param0: com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener): com.mikepenz.materialdrawer.DrawerBuilder;
                public withSliderBackgroundColorRes(param0: number): com.mikepenz.materialdrawer.DrawerBuilder;
            }
        }
    }
}

import androidviewViewOnClickListener = android.view.View.OnClickListener;
import androidsupportv4widgetDrawerLayoutLayoutParams = android.support.v4.widget.DrawerLayout.LayoutParams;
/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.DrawerBuilder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.interfaces.IDrawerItem.d.ts" />
/// <reference path="./java.lang.Boolean.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export class DrawerUtils {
                public static handleHeaderView(param0: com.mikepenz.materialdrawer.DrawerBuilder): void;
                public static fillStickyDrawerItemFooter(param0: com.mikepenz.materialdrawer.DrawerBuilder, param1: androidviewViewGroup, param2: androidviewViewOnClickListener): void;
                public static processDrawerLayoutParams(param0: com.mikepenz.materialdrawer.DrawerBuilder, param1: androidsupportv4widgetDrawerLayoutLayoutParams): androidsupportv4widgetDrawerLayoutLayoutParams;
                public static setStickyFooterSelection(param0: com.mikepenz.materialdrawer.DrawerBuilder, param1: number, param2: javalangBoolean): void;
                public static getDrawerItem(param0: javautilList, param1: number): com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
                public static getDrawerItem(param0: javautilList, param1: javalangObject): com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
                public static buildStickyDrawerItemFooter(param0: androidcontentContext, param1: com.mikepenz.materialdrawer.DrawerBuilder, param2: androidviewViewOnClickListener): androidviewViewGroup;
                public static getStickyFooterPositionByIdentifier(param0: com.mikepenz.materialdrawer.DrawerBuilder, param1: number): number;
                public static rebuildStickyFooterView(param0: com.mikepenz.materialdrawer.DrawerBuilder): void;
                public static handleFooterView(param0: com.mikepenz.materialdrawer.DrawerBuilder, param1: androidviewViewOnClickListener): void;
                public static onFooterDrawerItemClick(param0: com.mikepenz.materialdrawer.DrawerBuilder, param1: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem, param2: androidviewView, param3: javalangBoolean): void;
                public static getPositionByIdentifier(param0: com.mikepenz.materialdrawer.DrawerBuilder, param1: number): number;
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.support.v7.widget.RecyclerView.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./com.mikepenz.fastadapter.FastAdapter.d.ts" />
/// <reference path="./com.mikepenz.fastadapter.adapters.ItemAdapter.d.ts" />
/// <reference path="./com.mikepenz.fastadapter.listeners.OnClickListener.d.ts" />
/// <reference path="./com.mikepenz.fastadapter.listeners.OnLongClickListener.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.AccountHeader.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.Drawer.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.interfaces.ICrossfader.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.interfaces.IDrawerItem.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export class MiniDrawer {
                public static PROFILE: number;
                public static ITEM: number;
                public mAdapter: com.mikepenz.fastadapter.FastAdapter;
                public mItemAdapter: com.mikepenz.fastadapter.adapters.ItemAdapter;
                public withCrossFader(param0: com.mikepenz.materialdrawer.interfaces.ICrossfader): com.mikepenz.materialdrawer.MiniDrawer;
                public getCrossFader(): com.mikepenz.materialdrawer.interfaces.ICrossfader;
                public getAdapter(): com.mikepenz.fastadapter.FastAdapter;
                public createItems(): void;
                public onProfileClick(): void;
                public withIncludeSecondaryDrawerItems(param0: boolean): com.mikepenz.materialdrawer.MiniDrawer;
                public constructor();
                public getOnMiniDrawerItemOnClickListener(): com.mikepenz.fastadapter.listeners.OnClickListener;
                public withEnableSelectedMiniDrawerItemBackground(param0: boolean): com.mikepenz.materialdrawer.MiniDrawer;
                public withDrawer(param0: com.mikepenz.materialdrawer.Drawer): com.mikepenz.materialdrawer.MiniDrawer;
                public withInnerShadow(param0: boolean): com.mikepenz.materialdrawer.MiniDrawer;
                public withInRTL(param0: boolean): com.mikepenz.materialdrawer.MiniDrawer;
                public withOnMiniDrawerItemLongClickListener(param0: com.mikepenz.fastadapter.listeners.OnLongClickListener): com.mikepenz.materialdrawer.MiniDrawer;
                public onItemClick(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): boolean;
                public build(param0: androidcontentContext): androidviewView;
                public getDrawer(): com.mikepenz.materialdrawer.Drawer;
                public setSelection(param0: number): void;
                public withAccountHeader(param0: com.mikepenz.materialdrawer.AccountHeader): com.mikepenz.materialdrawer.MiniDrawer;
                public getAccountHeader(): com.mikepenz.materialdrawer.AccountHeader;
                public withOnMiniDrawerItemClickListener(param0: com.mikepenz.materialdrawer.MiniDrawer.OnMiniDrawerItemClickListener): com.mikepenz.materialdrawer.MiniDrawer;
                public getRecyclerView(): androidsupportv7widgetRecyclerView;
                public updateItem(param0: number): void;
                public getItemAdapter(): com.mikepenz.fastadapter.adapters.ItemAdapter;
                public getMiniDrawerType(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): number;
                public getOnMiniDrawerItemLongClickListener(): com.mikepenz.fastadapter.listeners.OnLongClickListener;
                public withOnMiniDrawerItemOnClickListener(param0: com.mikepenz.fastadapter.listeners.OnClickListener): com.mikepenz.materialdrawer.MiniDrawer;
                public generateMiniDrawerItem(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
                public withEnableProfileClick(param0: boolean): com.mikepenz.materialdrawer.MiniDrawer;
            }
            export namespace MiniDrawer {
                export class OnMiniDrawerItemClickListener {
                    /**
                     * Constructs a new instance of the com.mikepenz.materialdrawer.MiniDrawer$OnMiniDrawerItemClickListener interface with the provided implementation.
                     */
                    public constructor(implementation: {
                        onItemClick(param0: androidviewView, param1: number, param2: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem, param3: number): boolean;
                    });
                    public onItemClick(param0: androidviewView, param1: number, param2: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem, param3: number): boolean;
                }
            }
        }
    }
}

import androidcontentresColorStateList = android.content.res.ColorStateList;
/// <reference path="./android.content.res.ColorStateList.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.widget.TextView.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ColorHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.DimenHolder.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace holder {
                export class BadgeStyle {
                    public withCornersDp(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public getColorPressed(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public withMinWidth(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public withPaddingTopBottomPx(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public withPaddingTopBottomRes(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public withPaddingTopBottomDp(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public style(param0: androidwidgetTextView, param1: androidcontentresColorStateList): void;
                    public withColor(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public withCorners(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public getPaddingLeftRight(): com.mikepenz.materialdrawer.holder.DimenHolder;
                    public withPaddingLeftRightRes(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public getGradientDrawable(): number;
                    public getMinWidth(): com.mikepenz.materialdrawer.holder.DimenHolder;
                    public style(param0: androidwidgetTextView): void;
                    public withTextColorRes(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public constructor();
                    public getColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public withCorners(param0: com.mikepenz.materialdrawer.holder.DimenHolder): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public constructor(param0: number, param1: number);
                    public withPaddingLeftRightDp(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public getPaddingTopBottom(): com.mikepenz.materialdrawer.holder.DimenHolder;
                    public withColorPressedRes(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public withMinWidth(param0: com.mikepenz.materialdrawer.holder.DimenHolder): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public withPadding(param0: com.mikepenz.materialdrawer.holder.DimenHolder): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public withGradientDrawable(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public withPaddingLeftRightPx(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public constructor(param0: number, param1: number, param2: number, param3: number);
                    public withTextColor(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public withColorRes(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public withPadding(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public getCorners(): com.mikepenz.materialdrawer.holder.DimenHolder;
                    public getBadgeBackground(): androidgraphicsdrawableDrawable;
                    public getTextColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public withBadgeBackground(param0: androidgraphicsdrawableDrawable): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public withColorPressed(param0: number): com.mikepenz.materialdrawer.holder.BadgeStyle;
                }
            }
        }
    }
}

declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace holder {
                export class ColorHolder {
                    public static fromColorRes(param0: number): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public constructor();
                    public static fromColor(param0: number): com.mikepenz.materialdrawer.holder.ColorHolder;
                }
            }
        }
    }
}

declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace holder {
                export class DimenHolder {
                    public constructor();
                    public static fromResource(param0: number): com.mikepenz.materialdrawer.holder.DimenHolder;
                    public static fromDp(param0: number): com.mikepenz.materialdrawer.holder.DimenHolder;
                    public static fromPixel(param0: number): com.mikepenz.materialdrawer.holder.DimenHolder;
                }
            }
        }
    }
}

import androidnetUri = android.net.Uri;
import androidgraphicsBitmap = android.graphics.Bitmap;
/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Bitmap.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.net.Uri.d.ts" />
/// <reference path="./android.widget.ImageView.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace holder {
                export class ImageHolder {
                    public setIIcon(param0: com.mikepenz.iconics.typeface.IIcon): void;
                    public constructor(param0: androidnetUri);
                    public constructor(param0: androidgraphicsBitmap);
                    public getIIcon(): com.mikepenz.iconics.typeface.IIcon;
                    public static decideIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder, param1: androidcontentContext, param2: number, param3: boolean, param4: number): androidgraphicsdrawableDrawable;
                    public constructor(param0: string);
                    public decideIcon(param0: androidcontentContext, param1: number, param2: boolean, param3: number): androidgraphicsdrawableDrawable;
                    public static applyDecidedIconOrSetGone(param0: com.mikepenz.materialdrawer.holder.ImageHolder, param1: androidwidgetImageView, param2: number, param3: boolean, param4: number): void;
                    public constructor(param0: com.mikepenz.iconics.typeface.IIcon);
                    public applyTo(param0: androidwidgetImageView, param1: string): boolean;
                    public constructor(param0: androidgraphicsdrawableDrawable);
                    public constructor(param0: number);
                }
            }
        }
    }
}

/// <reference path="./java.lang.CharSequence.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace holder {
                export class StringHolder {
                    public constructor(param0: string);
                    public constructor(param0: number);
                }
            }
        }
    }
}

import javautilHashMap = java.util.HashMap;
import javautilCollection = java.util.Collection;
/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.ITypeface.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.Collection.d.ts" />
/// <reference path="./java.util.HashMap.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace icons {
                export class MaterialDrawerFont {
                    public getFontName(): string;
                    public getVersion(): string;
                    public getTypeface(param0: androidcontentContext): androidgraphicsTypeface;
                    public getMappingPrefix(): string;
                    public getIconCount(): number;
                    public getLicenseUrl(): string;
                    public getCharacters(): javautilHashMap;
                    public getAuthor(): string;
                    public getIcon(param0: string): com.mikepenz.iconics.typeface.IIcon;
                    public getUrl(): string;
                    public constructor();
                    public getLicense(): string;
                    public getDescription(): string;
                    public getIcons(): javautilCollection;
                }
                export namespace MaterialDrawerFont {
                    export class Icon {
                        public static mdf_arrow_drop_down: com.mikepenz.materialdrawer.icons.MaterialDrawerFont.Icon;
                        public static mdf_arrow_drop_up: com.mikepenz.materialdrawer.icons.MaterialDrawerFont.Icon;
                        public static mdf_expand_less: com.mikepenz.materialdrawer.icons.MaterialDrawerFont.Icon;
                        public static mdf_expand_more: com.mikepenz.materialdrawer.icons.MaterialDrawerFont.Icon;
                        public static mdf_person: com.mikepenz.materialdrawer.icons.MaterialDrawerFont.Icon;
                        public getFormattedName(): string;
                        public getTypeface(): com.mikepenz.iconics.typeface.ITypeface;
                        public getCharacter(): string;
                        public static values(): native.Array<com.mikepenz.materialdrawer.icons.MaterialDrawerFont.Icon>;
                        public getName(): string;
                        public static valueOf(param0: string): com.mikepenz.materialdrawer.icons.MaterialDrawerFont.Icon;
                    }
                }
            }
        }
    }
}

declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace interfaces {
                export class ICrossfader {
                    /**
                     * Constructs a new instance of the com.mikepenz.materialdrawer.interfaces.ICrossfader interface with the provided implementation.
                     */
                    public constructor(implementation: {
                        crossfade(): void;
                        isCrossfaded(): boolean;
                    });
                    public crossfade(): void;
                    public isCrossfaded(): boolean;
                }
            }
        }
    }
}

import androidwidgetCompoundButton = android.widget.CompoundButton;
/// <reference path="./android.widget.CompoundButton.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.interfaces.IDrawerItem.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace interfaces {
                export class OnCheckedChangeListener {
                    /**
                     * Constructs a new instance of the com.mikepenz.materialdrawer.interfaces.OnCheckedChangeListener interface with the provided implementation.
                     */
                    public constructor(implementation: {
                        onCheckedChanged(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem, param1: androidwidgetCompoundButton, param2: boolean): void;
                    });
                    public onCheckedChanged(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem, param1: androidwidgetCompoundButton, param2: boolean): void;
                }
            }
        }
    }
}

import androidsupportv7widgetRecyclerViewViewHolder = android.support.v7.widget.RecyclerView.ViewHolder;
/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.BadgeStyle.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export abstract class AbstractBadgeableDrawerItem extends com.mikepenz.materialdrawer.model.BaseDescribeableDrawerItem implements com.mikepenz.materialdrawer.model.interfaces.ColorfulBadgeable {
                    public mBadge: com.mikepenz.materialdrawer.holder.StringHolder;
                    public mBadgeStyle: com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public isSelectable(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public getBadge(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public getBadgeStyle(): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public constructor();
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withName(param0: number): javalangObject;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public isEnabled(): boolean;
                    public withBadge(param0: com.mikepenz.materialdrawer.holder.StringHolder): com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem;
                    public withBadge(param0: string): com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem;
                    public withName(param0: string): javalangObject;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public withBadge(param0: string): javalangObject;
                    public withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public getTypeface(): androidgraphicsTypeface;
                    public withBadge(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public withIcon(param0: number): javalangObject;
                    public bindView(param0: com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem.ViewHolder, param1: javautilList): void;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public equals(param0: number): boolean;
                    public withBadge(param0: number): com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public getTag(): javalangObject;
                    public withBadgeStyle(param0: com.mikepenz.materialdrawer.holder.BadgeStyle): javalangObject;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public withBadge(param0: number): javalangObject;
                    public getLayoutRes(): number;
                    public withBadgeStyle(param0: com.mikepenz.materialdrawer.holder.BadgeStyle): com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem.ViewHolder;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
                export namespace AbstractBadgeableDrawerItem {
                    export class ViewHolder extends com.mikepenz.materialdrawer.model.BaseViewHolder {
                        public constructor(param0: androidviewView);
                    }
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.interfaces.IDrawerItem.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.interfaces.OnPostBindViewListener.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export abstract class AbstractDrawerItem {
                    public mIdentifier: number;
                    public mTag: javalangObject;
                    public mEnabled: boolean;
                    public mSelected: boolean;
                    public mSelectable: boolean;
                    public mSelectedBackgroundAnimated: boolean;
                    public mOnDrawerItemClickListener: com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener;
                    public mOnPostBindViewListener: com.mikepenz.materialdrawer.model.interfaces.OnPostBindViewListener;
                    public mSubItems: javautilList;
                    public getType(): number;
                    public onPostBindView(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem, param1: androidviewView): void;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public withOnDrawerItemClickListener(param0: com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener): javalangObject;
                    public withParent(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
                    public isExpanded(): boolean;
                    public withSubItems(param0: javautilList): javalangObject;
                    public getOnPostBindViewListener(): com.mikepenz.materialdrawer.model.interfaces.OnPostBindViewListener;
                    public isSelectable(): boolean;
                    public isAutoExpanding(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public constructor();
                    public getSubItems(): javautilList;
                    public withIdentifier(param0: number): javalangObject;
                    public isEnabled(): boolean;
                    public hashCode(): number;
                    public detachFromWindow(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public withSelectable(param0: boolean): javalangObject;
                    public withPostOnBindViewListener(param0: com.mikepenz.materialdrawer.model.interfaces.OnPostBindViewListener): javalangObject;
                    public withIsExpanded(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public withSelectedBackgroundAnimated(param0: boolean): javalangObject;
                    public isSelectedBackgroundAnimated(): boolean;
                    public getTag(): javalangObject;
                    public attachToWindow(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public getOnDrawerItemClickListener(): com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener;
                    public withEnabled(param0: boolean): javalangObject;
                    public withSubItems(param0: native.Array<com.mikepenz.materialdrawer.model.interfaces.IDrawerItem>): javalangObject;
                    public failedToRecycle(param0: androidsupportv7widgetRecyclerViewViewHolder): boolean;
                    public getIdentifier(): number;
                    public getParent(): com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;
                    public getLayoutRes(): number;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public withSetSelected(param0: boolean): javalangObject;
                    public isSelected(): boolean;
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.interfaces.OnCheckedChangeListener.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export abstract class AbstractSwitchableDrawerItem extends com.mikepenz.materialdrawer.model.BaseDescribeableDrawerItem {
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public bindView(param0: com.mikepenz.materialdrawer.model.AbstractSwitchableDrawerItem.ViewHolder, param1: javautilList): void;
                    public withChecked(param0: boolean): com.mikepenz.materialdrawer.model.AbstractSwitchableDrawerItem;
                    public withCheckable(param0: boolean): com.mikepenz.materialdrawer.model.AbstractSwitchableDrawerItem;
                    public isSelectable(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public constructor();
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withName(param0: number): javalangObject;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public isEnabled(): boolean;
                    public isChecked(): boolean;
                    public withName(param0: string): javalangObject;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.AbstractSwitchableDrawerItem.ViewHolder;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public getTypeface(): androidgraphicsTypeface;
                    public withOnCheckedChangeListener(param0: com.mikepenz.materialdrawer.interfaces.OnCheckedChangeListener): com.mikepenz.materialdrawer.model.AbstractSwitchableDrawerItem;
                    public getOnCheckedChangeListener(): com.mikepenz.materialdrawer.interfaces.OnCheckedChangeListener;
                    public withIcon(param0: number): javalangObject;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public equals(param0: number): boolean;
                    public isSwitchEnabled(): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public getTag(): javalangObject;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public withSwitchEnabled(param0: boolean): com.mikepenz.materialdrawer.model.AbstractSwitchableDrawerItem;
                    public getLayoutRes(): number;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
                export namespace AbstractSwitchableDrawerItem {
                    export class ViewHolder extends com.mikepenz.materialdrawer.model.BaseViewHolder {
                    }
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.interfaces.OnCheckedChangeListener.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class AbstractToggleableDrawerItem extends com.mikepenz.materialdrawer.model.BaseDescribeableDrawerItem {
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public setToggleEnabled(param0: boolean): void;
                    public setOnCheckedChangeListener(param0: com.mikepenz.materialdrawer.interfaces.OnCheckedChangeListener): void;
                    public isSelectable(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public constructor();
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withName(param0: number): javalangObject;
                    public withOnCheckedChangeListener(param0: com.mikepenz.materialdrawer.interfaces.OnCheckedChangeListener): com.mikepenz.materialdrawer.model.AbstractToggleableDrawerItem;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public isEnabled(): boolean;
                    public setChecked(param0: boolean): void;
                    public withChecked(param0: boolean): com.mikepenz.materialdrawer.model.AbstractToggleableDrawerItem;
                    public isChecked(): boolean;
                    public withName(param0: string): javalangObject;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public getTypeface(): androidgraphicsTypeface;
                    public getOnCheckedChangeListener(): com.mikepenz.materialdrawer.interfaces.OnCheckedChangeListener;
                    public withIcon(param0: number): javalangObject;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public getTag(): javalangObject;
                    public withToggleEnabled(param0: boolean): com.mikepenz.materialdrawer.model.AbstractToggleableDrawerItem;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.AbstractToggleableDrawerItem.ViewHolder;
                    public getLayoutRes(): number;
                    public isToggleEnabled(): boolean;
                    public bindView(param0: com.mikepenz.materialdrawer.model.AbstractToggleableDrawerItem.ViewHolder, param1: javautilList): void;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
                export namespace AbstractToggleableDrawerItem {
                    export class ViewHolder extends com.mikepenz.materialdrawer.model.BaseViewHolder {
                    }
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ColorHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.BaseViewHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export abstract class BaseDescribeableDrawerItem extends com.mikepenz.materialdrawer.model.BaseDrawerItem {
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public withDescription(param0: number): javalangObject;
                    public bindViewHelper(param0: com.mikepenz.materialdrawer.model.BaseViewHolder): void;
                    public isSelectable(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public constructor();
                    public withDescriptionTextColor(param0: number): javalangObject;
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withName(param0: number): javalangObject;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public isEnabled(): boolean;
                    public withDescription(param0: string): javalangObject;
                    public withName(param0: string): javalangObject;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public getTypeface(): androidgraphicsTypeface;
                    public withIcon(param0: number): javalangObject;
                    public getDescription(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public getTag(): javalangObject;
                    public getDescriptionTextColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public getLayoutRes(): number;
                    public withDescriptionTextColorRes(param0: number): javalangObject;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
            }
        }
    }
}

import androidutilPair = android.util.Pair;
/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.content.res.ColorStateList.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.util.Pair.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ColorHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export abstract class BaseDrawerItem extends com.mikepenz.materialdrawer.model.AbstractDrawerItem implements com.mikepenz.materialdrawer.model.interfaces.Nameable, com.mikepenz.materialdrawer.model.interfaces.Iconable, com.mikepenz.materialdrawer.model.interfaces.Tagable, com.mikepenz.materialdrawer.model.interfaces.Typefaceable {
                    public icon: com.mikepenz.materialdrawer.holder.ImageHolder;
                    public selectedIcon: com.mikepenz.materialdrawer.holder.ImageHolder;
                    public name: com.mikepenz.materialdrawer.holder.StringHolder;
                    public iconTinted: boolean;
                    public selectedColor: com.mikepenz.materialdrawer.holder.ColorHolder;
                    public textColor: com.mikepenz.materialdrawer.holder.ColorHolder;
                    public selectedTextColor: com.mikepenz.materialdrawer.holder.ColorHolder;
                    public disabledTextColor: com.mikepenz.materialdrawer.holder.ColorHolder;
                    public iconColor: com.mikepenz.materialdrawer.holder.ColorHolder;
                    public selectedIconColor: com.mikepenz.materialdrawer.holder.ColorHolder;
                    public disabledIconColor: com.mikepenz.materialdrawer.holder.ColorHolder;
                    public typeface: androidgraphicsTypeface;
                    public colorStateList: androidutilPair;
                    public level: number;
                    public getType(): number;
                    public getSelectedIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public withIconColorRes(param0: number): javalangObject;
                    public getSelectedIconColor(param0: androidcontentContext): number;
                    public withIconTintingEnabled(param0: boolean): javalangObject;
                    public getDisabledTextColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public withSelectedColor(param0: number): javalangObject;
                    public withIconTinted(param0: boolean): javalangObject;
                    public getIconColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public constructor();
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withName(param0: number): javalangObject;
                    public withDisabledIconColor(param0: number): javalangObject;
                    public isEnabled(): boolean;
                    public withName(param0: string): javalangObject;
                    public withTintSelectedIcon(param0: boolean): javalangObject;
                    public getSelectedColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                    public withSelectedIcon(param0: number): javalangObject;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public getIconColor(param0: androidcontentContext): number;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public getTag(): javalangObject;
                    public withSelectedIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public getSelectedTextColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                    public getSelectedColor(param0: androidcontentContext): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public getSelectedTextColor(param0: androidcontentContext): number;
                    public getSelectedIconColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public getTextColorStateList(param0: number, param1: number): androidcontentresColorStateList;
                    public withTextColor(param0: number): javalangObject;
                    public withIconColor(param0: number): javalangObject;
                    public isIconTinted(): boolean;
                    public isSelectable(): boolean;
                    public withDisabledIconColorRes(param0: number): javalangObject;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public withSelectedIconColor(param0: number): javalangObject;
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withSelectedIconColorRes(param0: number): javalangObject;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public withDisabledTextColor(param0: number): javalangObject;
                    public getColor(param0: androidcontentContext): number;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public withSelectedTextColor(param0: number): javalangObject;
                    public getTypeface(): androidgraphicsTypeface;
                    public withSelectedTextColorRes(param0: number): javalangObject;
                    public getLevel(): number;
                    public withIcon(param0: number): javalangObject;
                    public withLevel(param0: number): javalangObject;
                    public getDisabledIconColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTextColorRes(param0: number): javalangObject;
                    public withDisabledTextColorRes(param0: number): javalangObject;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public withSelectedColorRes(param0: number): javalangObject;
                    public getLayoutRes(): number;
                    public getTextColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public generateView(param0: androidcontentContext): androidviewView;
                }
            }
        }
    }
}

/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.widget.ImageView.d.ts" />
/// <reference path="./android.widget.TextView.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class BaseViewHolder {
                    public view: androidviewView;
                    public icon: androidwidgetImageView;
                    public name: androidwidgetTextView;
                    public description: androidwidgetTextView;
                    public constructor(param0: androidviewView);
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.DimenHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class ContainerDrawerItem extends com.mikepenz.materialdrawer.model.AbstractDrawerItem {
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public withViewPosition(param0: com.mikepenz.materialdrawer.model.ContainerDrawerItem.Position): com.mikepenz.materialdrawer.model.ContainerDrawerItem;
                    public withDivider(param0: boolean): com.mikepenz.materialdrawer.model.ContainerDrawerItem;
                    public isSelectable(): boolean;
                    public withView(param0: androidviewView): com.mikepenz.materialdrawer.model.ContainerDrawerItem;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public withHeight(param0: com.mikepenz.materialdrawer.holder.DimenHolder): com.mikepenz.materialdrawer.model.ContainerDrawerItem;
                    public constructor();
                    public getViewPosition(): com.mikepenz.materialdrawer.model.ContainerDrawerItem.Position;
                    public getView(): androidviewView;
                    public isEnabled(): boolean;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public bindView(param0: com.mikepenz.materialdrawer.model.ContainerDrawerItem.ViewHolder, param1: javautilList): void;
                    public withSelectable(param0: boolean): javalangObject;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.ContainerDrawerItem.ViewHolder;
                    public equals(param0: javalangObject): boolean;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public getHeight(): com.mikepenz.materialdrawer.holder.DimenHolder;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public getTag(): javalangObject;
                    public getLayoutRes(): number;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
                export namespace ContainerDrawerItem {
                    export class Position {
                        public static TOP: com.mikepenz.materialdrawer.model.ContainerDrawerItem.Position;
                        public static BOTTOM: com.mikepenz.materialdrawer.model.ContainerDrawerItem.Position;
                        public static NONE: com.mikepenz.materialdrawer.model.ContainerDrawerItem.Position;
                        public static valueOf(param0: string): com.mikepenz.materialdrawer.model.ContainerDrawerItem.Position;
                        public static values(): native.Array<com.mikepenz.materialdrawer.model.ContainerDrawerItem.Position>;
                    }
                    export class ViewHolder {
                    }
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class DividerDrawerItem extends com.mikepenz.materialdrawer.model.AbstractDrawerItem {
                    public getType(): number;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public bindView(param0: com.mikepenz.materialdrawer.model.DividerDrawerItem.ViewHolder, param1: javautilList): void;
                    public isSelectable(): boolean;
                    public getTag(): javalangObject;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public constructor();
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.DividerDrawerItem.ViewHolder;
                    public isEnabled(): boolean;
                    public getLayoutRes(): number;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
                export namespace DividerDrawerItem {
                    export class ViewHolder {
                    }
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./android.widget.ImageView.d.ts" />
/// <reference path="./android.widget.TextView.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.BadgeStyle.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ColorHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class ExpandableBadgeDrawerItem extends com.mikepenz.materialdrawer.model.BaseDescribeableDrawerItem implements com.mikepenz.materialdrawer.model.interfaces.ColorfulBadgeable {
                    public arrowColor: com.mikepenz.materialdrawer.holder.ColorHolder;
                    public arrowRotationAngleStart: number;
                    public arrowRotationAngleEnd: number;
                    public mBadge: com.mikepenz.materialdrawer.holder.StringHolder;
                    public mBadgeStyle: com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public withOnDrawerItemClickListener(param0: com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener): javalangObject;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.ExpandableBadgeDrawerItem.ViewHolder;
                    public isSelectable(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public getBadge(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public getBadgeStyle(): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public constructor();
                    public withOnDrawerItemClickListener(param0: com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener): com.mikepenz.materialdrawer.model.ExpandableBadgeDrawerItem;
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withName(param0: number): javalangObject;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public isEnabled(): boolean;
                    public withBadgeStyle(param0: com.mikepenz.materialdrawer.holder.BadgeStyle): com.mikepenz.materialdrawer.model.ExpandableBadgeDrawerItem;
                    public withName(param0: string): javalangObject;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public withBadge(param0: string): javalangObject;
                    public withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public bindView(param0: com.mikepenz.materialdrawer.model.ExpandableBadgeDrawerItem.ViewHolder, param1: javautilList): void;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public getTypeface(): androidgraphicsTypeface;
                    public withBadge(param0: string): com.mikepenz.materialdrawer.model.ExpandableBadgeDrawerItem;
                    public withBadge(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public withBadge(param0: com.mikepenz.materialdrawer.holder.StringHolder): com.mikepenz.materialdrawer.model.ExpandableBadgeDrawerItem;
                    public withIcon(param0: number): javalangObject;
                    public withBadge(param0: number): com.mikepenz.materialdrawer.model.ExpandableBadgeDrawerItem;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public getTag(): javalangObject;
                    public getOnDrawerItemClickListener(): com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener;
                    public withBadgeStyle(param0: com.mikepenz.materialdrawer.holder.BadgeStyle): javalangObject;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public withBadge(param0: number): javalangObject;
                    public getLayoutRes(): number;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
                export namespace ExpandableBadgeDrawerItem {
                    export class ViewHolder extends com.mikepenz.materialdrawer.model.BaseViewHolder {
                        public arrow: androidwidgetImageView;
                        public badgeContainer: androidviewView;
                        public badge: androidwidgetTextView;
                        public constructor(param0: androidviewView);
                    }
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./android.widget.ImageView.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ColorHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class ExpandableDrawerItem extends com.mikepenz.materialdrawer.model.BaseDescribeableDrawerItem {
                    public arrowColor: com.mikepenz.materialdrawer.holder.ColorHolder;
                    public arrowRotationAngleStart: number;
                    public arrowRotationAngleEnd: number;
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public withOnDrawerItemClickListener(param0: com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener): javalangObject;
                    public withOnDrawerItemClickListener(param0: com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener): com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
                    public isSelectable(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public constructor();
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withName(param0: number): javalangObject;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public isEnabled(): boolean;
                    public withName(param0: string): javalangObject;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public withArrowRotationAngleEnd(param0: number): com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
                    public withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public withArrowRotationAngleStart(param0: number): com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public getTypeface(): androidgraphicsTypeface;
                    public withIcon(param0: number): javalangObject;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public withArrowColorRes(param0: number): com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
                    public getTag(): javalangObject;
                    public getOnDrawerItemClickListener(): com.mikepenz.materialdrawer.Drawer.OnDrawerItemClickListener;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public withArrowColor(param0: number): com.mikepenz.materialdrawer.model.ExpandableDrawerItem;
                    public getLayoutRes(): number;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.ExpandableDrawerItem.ViewHolder;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public bindView(param0: com.mikepenz.materialdrawer.model.ExpandableDrawerItem.ViewHolder, param1: javautilList): void;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
                export namespace ExpandableDrawerItem {
                    export class ViewHolder extends com.mikepenz.materialdrawer.model.BaseViewHolder {
                        public arrow: androidwidgetImageView;
                        public constructor(param0: androidviewView);
                    }
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.DimenHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.PrimaryDrawerItem.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.SecondaryDrawerItem.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class MiniDrawerItem extends com.mikepenz.materialdrawer.model.BaseDrawerItem {
                    public mCustomHeight: com.mikepenz.materialdrawer.holder.DimenHolder;
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public withCustomHeightRes(param0: number): com.mikepenz.materialdrawer.model.MiniDrawerItem;
                    public isSelectable(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public constructor();
                    public withEnableSelectedBackground(param0: boolean): com.mikepenz.materialdrawer.model.MiniDrawerItem;
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withName(param0: number): javalangObject;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public isEnabled(): boolean;
                    public withCustomHeightDp(param0: number): com.mikepenz.materialdrawer.model.MiniDrawerItem;
                    public withName(param0: string): javalangObject;
                    public bindView(param0: com.mikepenz.materialdrawer.model.MiniDrawerItem.ViewHolder, param1: javautilList): void;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public withCustomHeightPx(param0: number): com.mikepenz.materialdrawer.model.MiniDrawerItem;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public getTypeface(): androidgraphicsTypeface;
                    public constructor(param0: com.mikepenz.materialdrawer.model.SecondaryDrawerItem);
                    public withCustomHeight(param0: com.mikepenz.materialdrawer.holder.DimenHolder): com.mikepenz.materialdrawer.model.MiniDrawerItem;
                    public withIcon(param0: number): javalangObject;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public constructor(param0: com.mikepenz.materialdrawer.model.PrimaryDrawerItem);
                    public getTag(): javalangObject;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.MiniDrawerItem.ViewHolder;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public getLayoutRes(): number;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
                export namespace MiniDrawerItem {
                    export class ViewHolder {
                        public constructor(param0: androidviewView);
                    }
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Bitmap.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.net.Uri.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.DimenHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.ProfileDrawerItem.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class MiniProfileDrawerItem extends com.mikepenz.materialdrawer.model.AbstractDrawerItem implements com.mikepenz.materialdrawer.model.interfaces.IProfile {
                    public icon: com.mikepenz.materialdrawer.holder.ImageHolder;
                    public customHeight: com.mikepenz.materialdrawer.holder.DimenHolder;
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public withName(param0: string): com.mikepenz.materialdrawer.model.MiniProfileDrawerItem;
                    public bindView(param0: com.mikepenz.materialdrawer.model.MiniProfileDrawerItem.ViewHolder, param1: javautilList): void;
                    public isSelectable(): boolean;
                    public withCustomHeight(param0: com.mikepenz.materialdrawer.holder.DimenHolder): com.mikepenz.materialdrawer.model.MiniProfileDrawerItem;
                    public withCustomHeightDp(param0: number): com.mikepenz.materialdrawer.model.MiniProfileDrawerItem;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public constructor();
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withIcon(param0: string): com.mikepenz.materialdrawer.model.MiniProfileDrawerItem;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): com.mikepenz.materialdrawer.model.MiniProfileDrawerItem;
                    public isEnabled(): boolean;
                    public withEmail(param0: string): com.mikepenz.materialdrawer.model.MiniProfileDrawerItem;
                    public withIcon(param0: androidnetUri): javalangObject;
                    public withIcon(param0: androidnetUri): com.mikepenz.materialdrawer.model.MiniProfileDrawerItem;
                    public withName(param0: string): javalangObject;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public withIcon(param0: androidgraphicsBitmap): com.mikepenz.materialdrawer.model.MiniProfileDrawerItem;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public withCustomHeightPx(param0: number): com.mikepenz.materialdrawer.model.MiniProfileDrawerItem;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withCustomHeightRes(param0: number): com.mikepenz.materialdrawer.model.MiniProfileDrawerItem;
                    public withTag(param0: javalangObject): javalangObject;
                    public withIcon(param0: number): com.mikepenz.materialdrawer.model.MiniProfileDrawerItem;
                    public withIcon(param0: number): javalangObject;
                    public withEmail(param0: string): javalangObject;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public getTag(): javalangObject;
                    public constructor(param0: com.mikepenz.materialdrawer.model.ProfileDrawerItem);
                    public withIcon(param0: string): javalangObject;
                    public getEmail(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): com.mikepenz.materialdrawer.model.MiniProfileDrawerItem;
                    public withIcon(param0: androidgraphicsBitmap): javalangObject;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.MiniProfileDrawerItem.ViewHolder;
                    public getLayoutRes(): number;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
                export namespace MiniProfileDrawerItem {
                    export class ViewHolder {
                        public constructor(param0: androidviewView);
                    }
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.BadgeStyle.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class PrimaryDrawerItem extends com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem {
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public isSelectable(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public getBadgeStyle(): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public constructor();
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withName(param0: number): javalangObject;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public isEnabled(): boolean;
                    public withName(param0: string): javalangObject;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public getTypeface(): androidgraphicsTypeface;
                    public withIcon(param0: number): javalangObject;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public bindView(param0: com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem.ViewHolder, param1: javautilList): void;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public getTag(): javalangObject;
                    public withBadgeStyle(param0: com.mikepenz.materialdrawer.holder.BadgeStyle): javalangObject;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public getLayoutRes(): number;
                    public withBadgeStyle(param0: com.mikepenz.materialdrawer.holder.BadgeStyle): com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem.ViewHolder;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.content.res.ColorStateList.d.ts" />
/// <reference path="./android.graphics.Bitmap.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.net.Uri.d.ts" />
/// <reference path="./android.util.Pair.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ColorHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class ProfileDrawerItem extends com.mikepenz.materialdrawer.model.AbstractDrawerItem implements com.mikepenz.materialdrawer.model.interfaces.IProfile, com.mikepenz.materialdrawer.model.interfaces.Tagable, com.mikepenz.materialdrawer.model.interfaces.Typefaceable {
                    public nameShown: boolean;
                    public icon: com.mikepenz.materialdrawer.holder.ImageHolder;
                    public name: com.mikepenz.materialdrawer.holder.StringHolder;
                    public email: com.mikepenz.materialdrawer.holder.StringHolder;
                    public selectedColor: com.mikepenz.materialdrawer.holder.ColorHolder;
                    public textColor: com.mikepenz.materialdrawer.holder.ColorHolder;
                    public selectedTextColor: com.mikepenz.materialdrawer.holder.ColorHolder;
                    public disabledTextColor: com.mikepenz.materialdrawer.holder.ColorHolder;
                    public typeface: androidgraphicsTypeface;
                    public colorStateList: androidutilPair;
                    public getType(): number;
                    public withEmail(param0: string): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public withIcon(param0: number): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.ProfileDrawerItem.ViewHolder;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public withIcon(param0: androidnetUri): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public withIcon(param0: string): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public withName(param0: number): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public withNameShown(param0: boolean): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public getDisabledTextColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public constructor();
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public isEnabled(): boolean;
                    public withIcon(param0: androidnetUri): javalangObject;
                    public withSelectedColor(param0: number): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public withName(param0: string): javalangObject;
                    public getSelectedColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public withEmail(param0: string): javalangObject;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public getTag(): javalangObject;
                    public withIcon(param0: androidgraphicsBitmap): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public withIcon(param0: string): javalangObject;
                    public withName(param0: string): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public getEmail(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withEmail(param0: number): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public getSelectedTextColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public bindView(param0: com.mikepenz.materialdrawer.model.ProfileDrawerItem.ViewHolder, param1: javautilList): void;
                    public getSelectedTextColor(param0: androidcontentContext): number;
                    public getTextColorStateList(param0: number, param1: number): androidcontentresColorStateList;
                    public isSelectable(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withDisabledTextColorRes(param0: number): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public withTextColorRes(param0: number): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public withSelectedTextColorRes(param0: number): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public getColor(param0: androidcontentContext): number;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public withTextColor(param0: number): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public withDisabledTextColor(param0: number): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public withTypeface(param0: androidgraphicsTypeface): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public getTypeface(): androidgraphicsTypeface;
                    public withIcon(param0: number): javalangObject;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public withSelectedTextColor(param0: number): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public withSelectedColorRes(param0: number): com.mikepenz.materialdrawer.model.ProfileDrawerItem;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public isNameShown(): boolean;
                    public withIcon(param0: androidgraphicsBitmap): javalangObject;
                    public getLayoutRes(): number;
                    public getTextColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public generateView(param0: androidcontentContext): androidviewView;
                }
                export namespace ProfileDrawerItem {
                    export class ViewHolder {
                    }
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Bitmap.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.net.Uri.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ColorHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class ProfileSettingDrawerItem extends com.mikepenz.materialdrawer.model.AbstractDrawerItem implements com.mikepenz.materialdrawer.model.interfaces.IProfile, com.mikepenz.materialdrawer.model.interfaces.Tagable, com.mikepenz.materialdrawer.model.interfaces.Typefaceable {
                    public getType(): number;
                    public withTextColor(param0: number): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public setDescription(param0: string): void;
                    public withTextColorRes(param0: number): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public withDescription(param0: number): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public withDescription(param0: string): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public withSelectedColorRes(param0: number): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public withIcon(param0: string): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public getIconColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public constructor();
                    public withIcon(param0: androidnetUri): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public isEnabled(): boolean;
                    public withIcon(param0: androidnetUri): javalangObject;
                    public withName(param0: string): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public withName(param0: string): javalangObject;
                    public getSelectedColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public withEmail(param0: string): javalangObject;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public withIcon(param0: androidgraphicsBitmap): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public withIcon(param0: androidgraphicsdrawableDrawable): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public withDescriptionTextColor(param0: number): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public getTag(): javalangObject;
                    public withIcon(param0: string): javalangObject;
                    public getEmail(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: number): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public withSelectedColor(param0: number): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public withTypeface(param0: androidgraphicsTypeface): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public withEmail(param0: string): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public bindView(param0: com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem.ViewHolder, param1: javautilList): void;
                    public withDescriptionTextColorRes(param0: number): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public isIconTinted(): boolean;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem.ViewHolder;
                    public isSelectable(): boolean;
                    public withIconTinted(param0: boolean): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public setIconTinted(param0: boolean): void;
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withSelectable(param0: boolean): javalangObject;
                    public withSelectable(param0: boolean): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public equals(param0: javalangObject): boolean;
                    public getTypeface(): androidgraphicsTypeface;
                    public withIcon(param0: number): javalangObject;
                    public getDescription(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public withName(param0: number): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public getDescriptionTextColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public withIconColor(param0: number): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public withIconColorRes(param0: number): com.mikepenz.materialdrawer.model.ProfileSettingDrawerItem;
                    public withIcon(param0: androidgraphicsBitmap): javalangObject;
                    public getLayoutRes(): number;
                    public getTextColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public generateView(param0: androidcontentContext): androidviewView;
                }
                export namespace ProfileSettingDrawerItem {
                    export class ViewHolder {
                    }
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.BadgeStyle.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class SecondaryDrawerItem extends com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem {
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public isSelectable(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public getBadgeStyle(): com.mikepenz.materialdrawer.holder.BadgeStyle;
                    public constructor();
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withName(param0: number): javalangObject;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public isEnabled(): boolean;
                    public withName(param0: string): javalangObject;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public getColor(param0: androidcontentContext): number;
                    public withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public getTypeface(): androidgraphicsTypeface;
                    public withIcon(param0: number): javalangObject;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public bindView(param0: com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem.ViewHolder, param1: javautilList): void;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public getTag(): javalangObject;
                    public withBadgeStyle(param0: com.mikepenz.materialdrawer.holder.BadgeStyle): javalangObject;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public getLayoutRes(): number;
                    public withBadgeStyle(param0: com.mikepenz.materialdrawer.holder.BadgeStyle): com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.AbstractBadgeableDrawerItem.ViewHolder;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class SecondarySwitchDrawerItem extends com.mikepenz.materialdrawer.model.AbstractSwitchableDrawerItem {
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public bindView(param0: com.mikepenz.materialdrawer.model.AbstractSwitchableDrawerItem.ViewHolder, param1: javautilList): void;
                    public isSelectable(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public constructor();
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withName(param0: number): javalangObject;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public isEnabled(): boolean;
                    public withName(param0: string): javalangObject;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public getColor(param0: androidcontentContext): number;
                    public withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                    public withSelectable(param0: boolean): javalangObject;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.AbstractSwitchableDrawerItem.ViewHolder;
                    public equals(param0: javalangObject): boolean;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public getTypeface(): androidgraphicsTypeface;
                    public withIcon(param0: number): javalangObject;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public getTag(): javalangObject;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public getLayoutRes(): number;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class SecondaryToggleDrawerItem extends com.mikepenz.materialdrawer.model.AbstractToggleableDrawerItem {
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public isSelectable(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public constructor();
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withName(param0: number): javalangObject;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public isEnabled(): boolean;
                    public withName(param0: string): javalangObject;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public getColor(param0: androidcontentContext): number;
                    public withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public getTypeface(): androidgraphicsTypeface;
                    public withIcon(param0: number): javalangObject;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public getTag(): javalangObject;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.AbstractToggleableDrawerItem.ViewHolder;
                    public getLayoutRes(): number;
                    public bindView(param0: com.mikepenz.materialdrawer.model.AbstractToggleableDrawerItem.ViewHolder, param1: javautilList): void;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ColorHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class SectionDrawerItem extends com.mikepenz.materialdrawer.model.AbstractDrawerItem implements com.mikepenz.materialdrawer.model.interfaces.Nameable, com.mikepenz.materialdrawer.model.interfaces.Typefaceable {
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public isSelectable(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public constructor();
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withName(param0: number): javalangObject;
                    public isEnabled(): boolean;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public withName(param0: number): com.mikepenz.materialdrawer.model.SectionDrawerItem;
                    public withName(param0: string): javalangObject;
                    public withTextColorRes(param0: number): com.mikepenz.materialdrawer.model.SectionDrawerItem;
                    public bindView(param0: com.mikepenz.materialdrawer.model.SectionDrawerItem.ViewHolder, param1: javautilList): void;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public hasDivider(): boolean;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withDivider(param0: boolean): com.mikepenz.materialdrawer.model.SectionDrawerItem;
                    public withTag(param0: javalangObject): javalangObject;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): com.mikepenz.materialdrawer.model.SectionDrawerItem;
                    public getTypeface(): androidgraphicsTypeface;
                    public withTextColor(param0: number): com.mikepenz.materialdrawer.model.SectionDrawerItem;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public getTag(): javalangObject;
                    public withTypeface(param0: androidgraphicsTypeface): com.mikepenz.materialdrawer.model.SectionDrawerItem;
                    public withName(param0: string): com.mikepenz.materialdrawer.model.SectionDrawerItem;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public getLayoutRes(): number;
                    public getTextColor(): com.mikepenz.materialdrawer.holder.ColorHolder;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public isSelected(): boolean;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.SectionDrawerItem.ViewHolder;
                    public withSetSelected(param0: boolean): javalangObject;
                }
                export namespace SectionDrawerItem {
                    export class ViewHolder {
                    }
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class SwitchDrawerItem extends com.mikepenz.materialdrawer.model.AbstractSwitchableDrawerItem {
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public bindView(param0: com.mikepenz.materialdrawer.model.AbstractSwitchableDrawerItem.ViewHolder, param1: javautilList): void;
                    public isSelectable(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public constructor();
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withName(param0: number): javalangObject;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public isEnabled(): boolean;
                    public withName(param0: string): javalangObject;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                    public withSelectable(param0: boolean): javalangObject;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.AbstractSwitchableDrawerItem.ViewHolder;
                    public equals(param0: javalangObject): boolean;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public getTypeface(): androidgraphicsTypeface;
                    public withIcon(param0: number): javalangObject;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public getTag(): javalangObject;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public getLayoutRes(): number;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export class ToggleDrawerItem extends com.mikepenz.materialdrawer.model.AbstractToggleableDrawerItem {
                    public getType(): number;
                    public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    public isSelectable(): boolean;
                    public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                    public constructor();
                    public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                    public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    public withName(param0: number): javalangObject;
                    public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                    public isEnabled(): boolean;
                    public withName(param0: string): javalangObject;
                    public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                    public withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                    public withSelectable(param0: boolean): javalangObject;
                    public equals(param0: javalangObject): boolean;
                    public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                    public withTag(param0: javalangObject): javalangObject;
                    public getTypeface(): androidgraphicsTypeface;
                    public withIcon(param0: number): javalangObject;
                    public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                    public equals(param0: number): boolean;
                    public getViewHolder(param0: androidviewView): androidsupportv7widgetRecyclerViewViewHolder;
                    public getTag(): javalangObject;
                    public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                    public getViewHolder(param0: androidviewView): com.mikepenz.materialdrawer.model.AbstractToggleableDrawerItem.ViewHolder;
                    public getLayoutRes(): number;
                    public bindView(param0: com.mikepenz.materialdrawer.model.AbstractToggleableDrawerItem.ViewHolder, param1: javautilList): void;
                    public generateView(param0: androidcontentContext): androidviewView;
                    public isSelected(): boolean;
                    public withSetSelected(param0: boolean): javalangObject;
                }
            }
        }
    }
}

/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export namespace interfaces {
                    export class Badgeable {
                        /**
                         * Constructs a new instance of the com.mikepenz.materialdrawer.model.interfaces.Badgeable interface with the provided implementation.
                         */
                        public constructor(implementation: {
                            withBadge(param0: string): javalangObject;
                            withBadge(param0: number): javalangObject;
                            withBadge(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                            getBadge(): com.mikepenz.materialdrawer.holder.StringHolder;
                        });
                        public withBadge(param0: string): javalangObject;
                        public withBadge(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                        public withBadge(param0: number): javalangObject;
                        public getBadge(): com.mikepenz.materialdrawer.holder.StringHolder;
                    }
                }
            }
        }
    }
}

/// <reference path="./com.mikepenz.materialdrawer.holder.BadgeStyle.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export namespace interfaces {
                    export class ColorfulBadgeable {
                        /**
                         * Constructs a new instance of the com.mikepenz.materialdrawer.model.interfaces.ColorfulBadgeable interface with the provided implementation.
                         */
                        public constructor(implementation: {
                            withBadgeStyle(param0: com.mikepenz.materialdrawer.holder.BadgeStyle): javalangObject;
                            getBadgeStyle(): com.mikepenz.materialdrawer.holder.BadgeStyle;
                            withBadge(param0: string): javalangObject;
                            withBadge(param0: number): javalangObject;
                            withBadge(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                            getBadge(): com.mikepenz.materialdrawer.holder.StringHolder;
                        });
                        public withBadge(param0: string): javalangObject;
                        public withBadge(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                        public withBadge(param0: number): javalangObject;
                        public withBadgeStyle(param0: com.mikepenz.materialdrawer.holder.BadgeStyle): javalangObject;
                        public getBadgeStyle(): com.mikepenz.materialdrawer.holder.BadgeStyle;
                        public getBadge(): com.mikepenz.materialdrawer.holder.StringHolder;
                    }
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./android.view.ViewGroup.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.util.List.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export namespace interfaces {
                    export class IDrawerItem {
                        /**
                         * Constructs a new instance of the com.mikepenz.materialdrawer.model.interfaces.IDrawerItem interface with the provided implementation.
                         */
                        public constructor(implementation: {
                            getTag(): javalangObject;
                            isEnabled(): boolean;
                            isSelected(): boolean;
                            withSetSelected(param0: boolean): javalangObject;
                            isSelectable(): boolean;
                            withSelectable(param0: boolean): javalangObject;
                            getType(): number;
                            getLayoutRes(): number;
                            generateView(param0: androidcontentContext): androidviewView;
                            generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                            getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                            unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                            bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                            equals(param0: number): boolean;
                        });
                        public withSetSelected(param0: boolean): javalangObject;
                        public equals(param0: number): boolean;
                        public getTag(): javalangObject;
                        public isSelected(): boolean;
                        public getViewHolder(param0: androidviewViewGroup): androidsupportv7widgetRecyclerViewViewHolder;
                        public getType(): number;
                        public isEnabled(): boolean;
                        public getLayoutRes(): number;
                        public generateView(param0: androidcontentContext): androidviewView;
                        public unbindView(param0: androidsupportv7widgetRecyclerViewViewHolder): void;
                        public withSelectable(param0: boolean): javalangObject;
                        public isSelectable(): boolean;
                        public generateView(param0: androidcontentContext, param1: androidviewViewGroup): androidviewView;
                        public bindView(param0: androidsupportv7widgetRecyclerViewViewHolder, param1: javautilList): void;
                    }
                }
            }
        }
    }
}

/// <reference path="./android.graphics.Bitmap.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.net.Uri.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export namespace interfaces {
                    export class IProfile {
                        /**
                         * Constructs a new instance of the com.mikepenz.materialdrawer.model.interfaces.IProfile interface with the provided implementation.
                         */
                        public constructor(implementation: {
                            withName(param0: string): javalangObject;
                            getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                            withEmail(param0: string): javalangObject;
                            getEmail(): com.mikepenz.materialdrawer.holder.StringHolder;
                            withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                            withIcon(param0: androidgraphicsBitmap): javalangObject;
                            withIcon(param0: number): javalangObject;
                            withIcon(param0: string): javalangObject;
                            withIcon(param0: androidnetUri): javalangObject;
                            withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                            getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                            withSelectable(param0: boolean): javalangObject;
                            isSelectable(): boolean;
                        });
                        public withName(param0: string): javalangObject;
                        public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                        public getEmail(): com.mikepenz.materialdrawer.holder.StringHolder;
                        public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                        public withIcon(param0: androidnetUri): javalangObject;
                        public withEmail(param0: string): javalangObject;
                        public withIcon(param0: androidgraphicsBitmap): javalangObject;
                        public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                        public withIcon(param0: string): javalangObject;
                        public withSelectable(param0: boolean): javalangObject;
                        public isSelectable(): boolean;
                        public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                        public withIcon(param0: number): javalangObject;
                    }
                }
            }
        }
    }
}

/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./com.mikepenz.iconics.typeface.IIcon.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.ImageHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export namespace interfaces {
                    export class Iconable {
                        /**
                         * Constructs a new instance of the com.mikepenz.materialdrawer.model.interfaces.Iconable interface with the provided implementation.
                         */
                        public constructor(implementation: {
                            withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                            withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                            withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                            getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                        });
                        public getIcon(): com.mikepenz.materialdrawer.holder.ImageHolder;
                        public withIcon(param0: com.mikepenz.iconics.typeface.IIcon): javalangObject;
                        public withIcon(param0: com.mikepenz.materialdrawer.holder.ImageHolder): javalangObject;
                        public withIcon(param0: androidgraphicsdrawableDrawable): javalangObject;
                    }
                }
            }
        }
    }
}

/// <reference path="./com.mikepenz.materialdrawer.holder.StringHolder.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export namespace interfaces {
                    export class Nameable {
                        /**
                         * Constructs a new instance of the com.mikepenz.materialdrawer.model.interfaces.Nameable interface with the provided implementation.
                         */
                        public constructor(implementation: {
                            withName(param0: string): javalangObject;
                            withName(param0: number): javalangObject;
                            withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                            getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                        });
                        public withName(param0: string): javalangObject;
                        public withName(param0: number): javalangObject;
                        public withName(param0: com.mikepenz.materialdrawer.holder.StringHolder): javalangObject;
                        public getName(): com.mikepenz.materialdrawer.holder.StringHolder;
                    }
                }
            }
        }
    }
}

/// <reference path="./android.view.View.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.interfaces.IDrawerItem.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export namespace interfaces {
                    export class OnPostBindViewListener {
                        /**
                         * Constructs a new instance of the com.mikepenz.materialdrawer.model.interfaces.OnPostBindViewListener interface with the provided implementation.
                         */
                        public constructor(implementation: {
                            onBindView(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem, param1: androidviewView): void;
                        });
                        public onBindView(param0: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem, param1: androidviewView): void;
                    }
                }
            }
        }
    }
}

/// <reference path="./java.lang.Object.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export namespace interfaces {
                    export class Selectable {
                        /**
                         * Constructs a new instance of the com.mikepenz.materialdrawer.model.interfaces.Selectable interface with the provided implementation.
                         */
                        public constructor(implementation: {
                            withSelectable(param0: boolean): javalangObject;
                            isSelectable(): boolean;
                        });
                        public withSelectable(param0: boolean): javalangObject;
                        public isSelectable(): boolean;
                    }
                }
            }
        }
    }
}

/// <reference path="./java.lang.Object.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export namespace interfaces {
                    export class Tagable {
                        /**
                         * Constructs a new instance of the com.mikepenz.materialdrawer.model.interfaces.Tagable interface with the provided implementation.
                         */
                        public constructor(implementation: {
                            withTag(param0: javalangObject): javalangObject;
                            getTag(): javalangObject;
                        });
                        public withTag(param0: javalangObject): javalangObject;
                        public getTag(): javalangObject;
                    }
                }
            }
        }
    }
}

/// <reference path="./android.graphics.Typeface.d.ts" />
/// <reference path="./java.lang.Object.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export namespace interfaces {
                    export class Typefaceable {
                        /**
                         * Constructs a new instance of the com.mikepenz.materialdrawer.model.interfaces.Typefaceable interface with the provided implementation.
                         */
                        public constructor(implementation: {
                            withTypeface(param0: androidgraphicsTypeface): javalangObject;
                            getTypeface(): androidgraphicsTypeface;
                        });
                        public withTypeface(param0: androidgraphicsTypeface): javalangObject;
                        public getTypeface(): androidgraphicsTypeface;
                    }
                }
            }
        }
    }
}

import androidgraphicsdrawableStateListDrawable = android.graphics.drawable.StateListDrawable;
/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.drawable.StateListDrawable.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.holder.BadgeStyle.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace model {
                export namespace utils {
                    export class BadgeDrawableBuilder {
                        public constructor(param0: com.mikepenz.materialdrawer.holder.BadgeStyle);
                        public build(param0: androidcontentContext): androidgraphicsdrawableStateListDrawable;
                    }
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.net.Uri.d.ts" />
/// <reference path="./android.widget.ImageView.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace util {
                export abstract class AbstractDrawerImageLoader {
                    public placeholder(param0: androidcontentContext, param1: string): androidgraphicsdrawableDrawable;
                    public set(param0: androidwidgetImageView, param1: androidnetUri, param2: androidgraphicsdrawableDrawable, param3: string): void;
                    public constructor();
                    public set(param0: androidwidgetImageView, param1: androidnetUri, param2: androidgraphicsdrawableDrawable): void;
                    public placeholder(param0: androidcontentContext): androidgraphicsdrawableDrawable;
                    public cancel(param0: androidwidgetImageView): void;
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.net.Uri.d.ts" />
/// <reference path="./android.widget.ImageView.d.ts" />
/// <reference path="./java.lang.String.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace util {
                export class DrawerImageLoader {
                    public static getInstance(): com.mikepenz.materialdrawer.util.DrawerImageLoader;
                    public cancelImage(param0: androidwidgetImageView): void;
                    public withHandleAllUris(param0: boolean): com.mikepenz.materialdrawer.util.DrawerImageLoader;
                    public static init(param0: com.mikepenz.materialdrawer.util.DrawerImageLoader.IDrawerImageLoader): com.mikepenz.materialdrawer.util.DrawerImageLoader;
                    public getImageLoader(): com.mikepenz.materialdrawer.util.DrawerImageLoader.IDrawerImageLoader;
                    public setImageLoader(param0: com.mikepenz.materialdrawer.util.DrawerImageLoader.IDrawerImageLoader): void;
                    public setImage(param0: androidwidgetImageView, param1: androidnetUri, param2: string): boolean;
                }
                export namespace DrawerImageLoader {
                    export class IDrawerImageLoader {
                        /**
                         * Constructs a new instance of the com.mikepenz.materialdrawer.util.DrawerImageLoader$IDrawerImageLoader interface with the provided implementation.
                         */
                        public constructor(implementation: {
                            set(param0: androidwidgetImageView, param1: androidnetUri, param2: androidgraphicsdrawableDrawable): void;
                            set(param0: androidwidgetImageView, param1: androidnetUri, param2: androidgraphicsdrawableDrawable, param3: string): void;
                            cancel(param0: androidwidgetImageView): void;
                            placeholder(param0: androidcontentContext): androidgraphicsdrawableDrawable;
                            placeholder(param0: androidcontentContext, param1: string): androidgraphicsdrawableDrawable;
                        });
                        public set(param0: androidwidgetImageView, param1: androidnetUri, param2: androidgraphicsdrawableDrawable, param3: string): void;
                        public set(param0: androidwidgetImageView, param1: androidnetUri, param2: androidgraphicsdrawableDrawable): void;
                        public placeholder(param0: androidcontentContext): androidgraphicsdrawableDrawable;
                        public placeholder(param0: androidcontentContext, param1: string): androidgraphicsdrawableDrawable;
                        public cancel(param0: androidwidgetImageView): void;
                    }
                    export class Tags {
                        public static PROFILE: com.mikepenz.materialdrawer.util.DrawerImageLoader.Tags;
                        public static PROFILE_DRAWER_ITEM: com.mikepenz.materialdrawer.util.DrawerImageLoader.Tags;
                        public static ACCOUNT_HEADER: com.mikepenz.materialdrawer.util.DrawerImageLoader.Tags;
                        public static valueOf(param0: string): com.mikepenz.materialdrawer.util.DrawerImageLoader.Tags;
                        public static values(): native.Array<com.mikepenz.materialdrawer.util.DrawerImageLoader.Tags>;
                    }
                }
            }
        }
    }
}

import javautilArrayList = java.util.ArrayList;
/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.view.View.d.ts" />
/// <reference path="./com.mikepenz.materialdrawer.model.interfaces.IDrawerItem.d.ts" />
/// <reference path="./java.util.ArrayList.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace util {
                export class DrawerItemViewHelper {
                    public withDrawerItems(param0: native.Array<com.mikepenz.materialdrawer.model.interfaces.IDrawerItem>): com.mikepenz.materialdrawer.util.DrawerItemViewHelper;
                    public withDivider(param0: boolean): com.mikepenz.materialdrawer.util.DrawerItemViewHelper;
                    public constructor(param0: androidcontentContext);
                    public withOnDrawerItemClickListener(param0: com.mikepenz.materialdrawer.util.DrawerItemViewHelper.OnDrawerItemClickListener): com.mikepenz.materialdrawer.util.DrawerItemViewHelper;
                    public build(): androidviewView;
                    public withDrawerItems(param0: javautilArrayList): com.mikepenz.materialdrawer.util.DrawerItemViewHelper;
                }
                export namespace DrawerItemViewHelper {
                    export class OnDrawerItemClickListener {
                        /**
                         * Constructs a new instance of the com.mikepenz.materialdrawer.util.DrawerItemViewHelper$OnDrawerItemClickListener interface with the provided implementation.
                         */
                        public constructor(implementation: {
                            onItemClick(param0: androidviewView, param1: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): void;
                        });
                        public onItemClick(param0: androidviewView, param1: com.mikepenz.materialdrawer.model.interfaces.IDrawerItem): void;
                    }
                }
            }
        }
    }
}

/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.content.res.ColorStateList.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.graphics.drawable.StateListDrawable.d.ts" />
/// <reference path="./android.view.View.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace util {
                export class DrawerUIUtils {
                    public static getIconStateList(param0: androidgraphicsdrawableDrawable, param1: androidgraphicsdrawableDrawable): androidgraphicsdrawableStateListDrawable;
                    public static getOptimalDrawerWidth(param0: androidcontentContext): number;
                    public constructor();
                    public static getDrawerItemBackground(param0: number): androidgraphicsdrawableStateListDrawable;
                    public static setDrawerVerticalPadding(param0: androidviewView, param1: number): void;
                    public static isSystemBarOnBottom(param0: androidcontentContext): boolean;
                    public static getTextColorStateList(param0: number, param1: number): androidcontentresColorStateList;
                    public static getPlaceHolder(param0: androidcontentContext): androidgraphicsdrawableDrawable;
                    public static setDrawerVerticalPadding(param0: androidviewView): void;
                }
            }
        }
    }
}

/// <reference path="./android.app.Activity.d.ts" />
/// <reference path="./android.view.View.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace util {
                export class KeyboardUtil {
                    public disable(): void;
                    public constructor(param0: androidappActivity, param1: androidviewView);
                    public static hideKeyboard(param0: androidappActivity): void;
                    public enable(): void;
                }
            }
        }
    }
}

/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace util {
                export class PressedEffectStateListDrawable {
                    public onStateChange(param0: native.Array<number>): boolean;
                    public isStateful(): boolean;
                    public constructor(param0: androidgraphicsdrawableDrawable, param1: number, param2: number);
                }
            }
        }
    }
}

import androidutilAttributeSet = android.util.AttributeSet;
import androidgraphicsCanvas = android.graphics.Canvas;
import androidviewMotionEvent = android.view.MotionEvent;
import androidgraphicsOutline = android.graphics.Outline;
/// <reference path="./android.content.Context.d.ts" />
/// <reference path="./android.graphics.Bitmap.d.ts" />
/// <reference path="./android.graphics.Canvas.d.ts" />
/// <reference path="./android.graphics.Outline.d.ts" />
/// <reference path="./android.graphics.drawable.Drawable.d.ts" />
/// <reference path="./android.net.Uri.d.ts" />
/// <reference path="./android.util.AttributeSet.d.ts" />
/// <reference path="./android.view.MotionEvent.d.ts" />
/// <reference path="./android.view.View.d.ts" />
declare namespace com {
    export namespace mikepenz {
        export namespace materialdrawer {
            export namespace view {
                export class BezelImageView {
                    public invalidateDrawable(param0: androidgraphicsdrawableDrawable): void;
                    public constructor(param0: androidcontentContext, param1: androidutilAttributeSet, param2: number);
                    public setImageURI(param0: androidnetUri): void;
                    public setFrame(param0: number, param1: number, param2: number, param3: number): boolean;
                    public onDraw(param0: androidgraphicsCanvas): void;
                    public setImageBitmap(param0: androidgraphicsBitmap): void;
                    public onSizeChanged(param0: number, param1: number, param2: number, param3: number): void;
                    public setSelectorColor(param0: number): void;
                    public dispatchTouchEvent(param0: androidviewMotionEvent): boolean;
                    public constructor(param0: androidcontentContext, param1: androidutilAttributeSet);
                    public disableTouchFeedback(param0: boolean): void;
                    public verifyDrawable(param0: androidgraphicsdrawableDrawable): boolean;
                    public setImageResource(param0: number): void;
                    public constructor(param0: androidcontentContext);
                    public drawableStateChanged(): void;
                    public setImageDrawable(param0: androidgraphicsdrawableDrawable): void;
                }
                export namespace BezelImageView {
                    export class CustomOutline {
                        public getOutline(param0: androidviewView, param1: androidgraphicsOutline): void;
                    }
                }
            }
        }
    }
}

