import { Observable } from 'tns-core-modules/data/observable';
import { TnsSideDrawer } from 'nativescript-foss-sidedrawer';

export class HelloWorldModel extends Observable {
  private _i: number = 0;

  constructor() {
    super();

    const listener = (index) => {
    };

    setTimeout(() => {
    TnsSideDrawer.build({
      templates: [{
        title: 'Home',
      }],
      title: 'This App Name',
      subtitle: 'is just as awesome as this subtitle!',
      listener: listener,
      context: this,
    });
    }, 1000);
    setTimeout(() => {
    TnsSideDrawer.build({
      templates: [{
        title: 'Home',
      }],
      title: 'This App Name',
      subtitle: 'is just as awesome as this subtitle!',
      listener: listener,
      context: this,
    });
    }, 3000);
  }
}
